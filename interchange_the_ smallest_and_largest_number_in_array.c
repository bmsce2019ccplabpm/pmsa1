#include<stdio.h>
int main()
{
    int a[20],n,i,temp;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    printf("Enter the array elements\n");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    int small = a[0],small_loc = 0,large = a[0],large_loc = 0;
    for(int i=0;i<n;i++)
    {
        if(a[i]<small)
        {
            small = a[i];
            small_loc = i;
        }
        if(a[i]>large)
        {
            large = a[i];
            large_loc = i;
        }
    }
    temp=a[large_loc];
    a[large_loc] = a[small_loc];
    a[small_loc] = temp;
    printf("The array after interchanging the largest and the smallest number is \n");
    for(int i = 0;i < n;i++)
        printf("%d  ",a[i]);
    return 0;
}